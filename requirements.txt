coverage==4.5.4
Django==2.2.6
pytz==2019.3
selenium==3.141.0
sqlparse==0.3.0
urllib3==1.25.6
whitenoise==4.1.4
gunicorn