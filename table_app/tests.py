from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.views import *
from.views import *


class UnitTest(TestCase):

    def test_url_exists(self):
        response = Client().get("/story8")
        self.assertEqual(response.status_code,200)

    def test_func_exec(self):
        response = resolve("/story8")
        self.assertEqual(response.func,tableView)

    def test_template_exists(self):
        response = Client().get("/story8")
        self.assertTemplateUsed(response,"main.html")

# Create your tests here.
