from django import forms
from django.forms import widgets

class SearchForm(forms.Form):
    search_key = forms.CharField(max_length = 300)